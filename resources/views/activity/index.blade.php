@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Daftar Aktivitas</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                            <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Buat Pesanan</button>
                    </div>
                    <div style="overflow-x:auto;">
                            <table class="table">
                                <thead>
                                        <tr>
                                                <th>Nama Aktivitas</th>
                                                <th>Tanggal</th>
                                                
                                            </tr>
                                </thead>
                              <tbody>
                                  @foreach($activities as $activity)
                                    <tr>
                                        <td>{{$activity->name}}</td>
                                        <td>{{$activity->created_at}}</td>
                                        
                                       
                                        
                                    </tr>
                                  @endforeach
                              </tbody>
                            </table>
                           
                          </div>
                </div>
                <!-- Trigger the modal with a button -->

<!-- Modal -->

            </div>
        </div>
    </div>
</div>
<!-- Modal -->

@endsection
