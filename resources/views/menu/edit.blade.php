@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Daftar Menu</div>

                <div class="card-body">
                        <form action="{{route('menu.update')}}" method="post">
                                <div class="modal-body">
                                
                    
                                   
                                    {{ csrf_field() }}
                                    
                                    <div class="form-group">
                                        <label for="">Nama Makanan</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Harga</label>
                                        <input type="text" class="form-control" name="harga">
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  
                                </div>
                            </form>
                    
                </div>
                <!-- Trigger the modal with a button -->

<!-- Modal -->

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            
          </div>
        </div>
</div>

@endsection
