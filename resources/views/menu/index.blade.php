@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Daftar Menu</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                            <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Tambah Menu</button>
                    </div>
                    <div style="overflow-x:auto;">
                            <table class="table">
                                <thead>
                                        <tr>
                                                <th>Nama Makanan</th>
                                                <th>Status</th>
                                                <th>Harga</th>
                                                <th>Di Input Oleh</th>
                                    
                                            </tr>
                                </thead>
                              <tbody>
                                  @foreach($menu as $men)
                                    <tr>
                                        
                                        
                                        <td><a href="{{url('edit').'/'.$men->id}}">{{$men->name}}</a></td>
                                        <td>{{$men->status}}</td>
                                        <td>{{$men->harga}}</td>
                                        <td>{{$men->users}}</td>
                                        <td>
                                            @if($men->status=='ready')
                                            <a href="" class="btn btn-primary">Not Ready</a>
                                            @else
                                            <a href="" class="btn btn-primary">Ready</a>
                                            @endif
                                            <a href="" class="btn btn-danger">Hapus</a></td>
                                        
                                    </tr>
                                  @endforeach
                              </tbody>
                            </table>
                            Halaman : {{ $menu->currentPage() }} <br/>
	                        Jumlah Data : {{ $menu->total() }} <br/>
	                        Data Per Halaman : {{ $menu->perPage() }} <br/>
 
 
	                        {{ $menu->links() }}
                          </div>
                </div>
                <!-- Trigger the modal with a button -->

<!-- Modal -->

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <form action="{{route('menu.create')}}" method="post">
            <div class="modal-body">
            

               
                {{ csrf_field() }}
                <div class="form-group">
                        <label for="">Nama Menu</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <input type="text" class="form-control" name="status">
                </div>
                <div class="form-group">
                    <label for="">Harga</label>
                    <input type="text" class="form-control" name="harga">
                </div>
              
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              
            </div>
        </form>
          </div>
        </div>
</div>
<div id="detailPesanan" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            
            <div class="modal-body">
            

        
          </div>
        </div>
</div>
@endsection
