@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Detail Pesanan {{$id}}</div>

                <div class="card-body">
                    
                    <div class="form-group">
                            <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Tambah Item</button>
                    </div>
                    <div style="overflow-x:auto;">
                            <table class="table">
                                <thead>
                                        <tr>
                                                <th>Nama makanan</th>
                                                <th>Harga</th>
                                                <th>Qty</th>
                                                
                                            
                                            </tr>
                                </thead>
                              <tbody>
                                  @foreach($detail as $det)
                                    <tr>
                                        
                                       
                                        <td>{{$det->menu_name}}</td>
                                        <td>{{$det->harga}}</td>
                                        <td>{{$det->qty}}</td>
                                        
                                        
                                        
                                    </tr>
                                  @endforeach
                              </tbody>
                              <tfoot>
                                <tr>
                                  <td></td>
                                  <td>Total :{{$total}}</td>
                                  <td><button class="btn btn-primary">Proses Pembayaran</button></td>
                                </tr>
                              </tfoot>
                            </table>
                            
                          </div>
                </div>
                <!-- Trigger the modal with a button -->

<!-- Modal -->

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <form action="{{route('add.detail')}}" method="post">
            <div class="modal-body">
            

                {{ csrf_field() }}
              <input type="text" name="pesanan" value={{$id}} hidden>
              <div class="form-group">
                  <label for="">Nama Makanan</label>
                  <select class="form-control" name="menu" id="">
                      @foreach($menus as $menu)
                  <option value="{{$menu->id}}">{{$menu->name}}</option>
                      @endforeach
                  </select>
                  
              </div>
              <div class="form-group">
                  <label for="">Jumlah / Qty</label>
                  <input type="number" class="form-control" name="qty">
              </div>
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              
            </div>
        </form>
          </div>
        </div>
</div>

<div id="pembayaran" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <form action="{{route('add.detail')}}" method="post">
      <div class="modal-body">
      

          {{ csrf_field() }}
        <input type="text" name="pesanan" value={{$id}} hidden>
        <div class="form-group">
            <label for="">Nama Makanan</label>
            <select class="form-control" name="menu" id="">
                @foreach($menus as $menu)
            <option value="{{$menu->id}}">{{$menu->name}}</option>
                @endforeach
            </select>
            
        </div>
        <div class="form-group">
            <label for="">Jumlah / Qty</label>
            <input type="number" class="form-control" name="qty">
        </div>
      </div>
      <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        
      </div>
  </form>
    </div>
  </div>
</div>

@endsection

