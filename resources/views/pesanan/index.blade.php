@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Daftar Pesanan</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                            <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Buat Pesanan</button>
                    </div>
                    <div style="overflow-x:auto;">
                            <table class="table">
                                <thead>
                                        <tr>
                                                <th>Nomor Pesanan</th>
                                                <th>Nama Pesanan</th>
                                                <th>Nomor Meja</th>
                                                <th>Status Pesanan</th>
                                                
                                                <th>Nama Pelayan</th>
                                                <th>Action</th>
                                            </tr>
                                </thead>
                              <tbody>
                                  @foreach($pesanan as $pesan)
                                    <tr>
                                        
                                        <td><a href="{{url('detailpesanan/').'/'.$pesan->nomor_pesanan}}">{{$pesan->nomor_pesanan}}</a></td>
                                        <td>{{$pesan->name}}</td>
                                        <td>{{$pesan->nomor_meja}}</td>
                                        <td>{{$pesan->status}}</td>
                                        <td>{{$pesan->user}}</td>
                                        <td><a href="" class="btn btn-success">Bayar</a> <a href="" class="btn btn-primary">Edit</a> <a href="" class="btn btn-danger">Cancel</a></td>
                                        
                                    </tr>
                                  @endforeach
                              </tbody>
                            </table>
                            Halaman : {{ $pesanan->currentPage() }} <br/>
	                        Jumlah Data : {{ $pesanan->total() }} <br/>
	                        Data Per Halaman : {{ $pesanan->perPage() }} <br/>
 
 
	                        {{ $pesanan->links() }}
                          </div>
                </div>
                <!-- Trigger the modal with a button -->

<!-- Modal -->

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <form action="{{route('pesanan.create')}}" method="post">
            <div class="modal-body">
            

                {{ csrf_field() }}
              <div class="form-group">
                  <label for="">Nomor Pesanan</label>
                  <input type="text" name="nomor_pesanan" class="form-control" value="{{'ERP'.date('dmY').'-'.sprintf("%'03d",$last+1)}}" readonly id="">
              </div>
              <div class="form-group">
                  <label for="">Nama Pesanan</label>
                  <input type="text" class="form-control" name="nama_pesanan">
              </div>
              <div class="form-group">
                  <label for="">Nomor Meja</label>
                  <input type="text" class="form-control" name="nomor_meja">
              </div>
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              
            </div>
        </form>
          </div>
        </div>
</div>
<div id="detailPesanan" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            
            <div class="modal-body">
            

        
          </div>
        </div>
</div>
@endsection
<script>
function getDetailPesanan(id) {
  $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: '{{url('detailpesanan')}}'+'/'+id,
        dataType: 'json',
        success: function (data) {
            console.log(url);
            container.html('');
            $.each(data, function(index, item) {
            container.html(''); //clears container for new data
            $.each(data, function(i, item) {
                  container.append('<div class="row"><div class="ten columns"><div class="editbuttoncont"><button class="btntimestampnameedit" data-seek="' + item.timestamp_time + '">' +  new Date(item.timestamp_time * 1000).toUTCString().match(/(\d\d:\d\d:\d\d)/)[0] +' - '+ item.timestamp_name +'</button></div></div> <div class="one columns"><form action="'+ item.timestamp_id +'/deletetimestamp" method="POST">' + '{!! csrf_field() !!}' +'<input type="hidden" name="_method" value="DELETE"><button class="btntimestampdelete"><i aria-hidden="true" class="fa fa-trash buttonicon"></i></button></form></div></div>');
          });
                container.append('<br>');
            });
        },error:function(){ 
             console.log(data);
        }
    });
}
 
</script>
