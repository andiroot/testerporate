<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(\App\Models\User::class, 2)->create();
        DB::table('role')->insert([
        	'name' => 'kasir',
        ]);
        DB::table('role')->insert([
        	'name' => 'pelayan',
        ]);
        DB::table('user_role')->insert([
            'user_id' => 1,
            'role_id'=>1
        ]);


        DB::table('user_role')->insert([
            'user_id' => 2,
            'role_id'=>2
        ]);


        DB::table('menu')->insert([
            'name' => 'nazgor',
            'status'=>'ready',
            'harga'=>10000,
            'created_by'=>1,
            
        ]);
        DB::table('menu')->insert([
            'name' => 'rendang badak',
            'status'=>'not ready',
            'harga'=>23000,
            'created_by'=>1,
            
        ]);
        DB::table('menu')->insert([
            'name' => 'kambing guling',
            'status'=>'ready',
            'harga'=>15000,
            'created_by'=>1,
            
        ]);
    }
}
