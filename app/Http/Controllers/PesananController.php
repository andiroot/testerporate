<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pesanan;
use App\Models\Pembayaran;
use App\Models\DetailPesanan;
use Auth;
use DB;
class PesananController extends Controller
{
    //
    public function index()
    {
        $pesanan=new Pesanan();
        $get=DB::table('pesanan')->join('users','users.id','pesanan.created_by')
                ->leftJoin('pembayaran','pembayaran.id','pesanan.pembayaran_id')
                ->select('users.name as user','pesanan.name','pesanan.nomor_meja',
                        'pesanan.nomor_pesanan','pesanan.status','pembayaran.dibayar')
                ->orderBy('pesanan.created_at','desc')
                        ->paginate(5);
                
        $last=Pesanan::orderBy('id','desc')->first();
        if($last==null){
            $last=0;
        }
        else{
            $last=$last->id;
        }
        
        return view('pesanan.index')->with('pesanan',$get)
            ->with('last',$last);
    }

    public function store(Request $request)
    {
        
        $data=[
            'nomor_pesanan'=>$request->nomor_pesanan,
            'created_by'=>Auth::id(),
            'nomor_meja'=>$request->nomor_meja,
            'name'=>$request->nama_pesanan,
            'status'=>'waiting',

        ];
        ///dd($data);

        Pesanan::create($data);
        $pesanan=Pesanan::where('nomor_pesanan',$request->nomor_pesanan)->first();
        DetailPesanan::create(['pesanan_id'=>$pesanan->id]);
        return redirect()->back();
    }
    
    public function show($id)
    {
        $get= DB::table('detail_pesanan')
            ->join('pesanan','pesanan.id','detail_pesanan.pesanan_id')
            ->leftJoin('menu','menu.id','detail_pesanan.menu_id')
            ->where('pesanan.nomor_pesanan',$id)
            ->select('menu.name as menu_name','menu.status','menu.harga','detail_pesanan.qty')
            ->get();

        $pesanan=Pesanan::where('nomor_pesanan',$id)->first();
        $total=DB::select(DB::raw('select sum(menu.harga*detail_pesanan.qty) from detail_pesanan left join menu on menu.id=detail_pesanan.menu_id 
        where detail_pesanan.pesanan_id=?'),[$pesanan->id]);
        
        $menu=DB::table('menu')->where('status','ready')->get();
        return view('pesanan.detail')->with('detail',$get)
                ->with('id',$id)
                ->with('menus',$menu)
                ->with('total',$total[0]->sum);
    }
    
    public function addmenu(Request $request)
    {
        $pesanan=Pesanan::where('nomor_pesanan',$request->pesanan)->first();
        
        $data=[
            'menu_id'=>$request->menu,
            'pesanan_id'=>$pesanan->id,
            'qty'=>$request->qty
        ];
        DetailPesanan::create($data);
        return redirect()->back();
    }
}
