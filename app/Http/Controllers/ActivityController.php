<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class ActivityController extends Controller
{
    //
    public function index()
    {
        $activity=DB::table('activity')->join('users','users.id','activity.user_id')
            ->select('activity.name','activity.created_at')->where('users.id',Auth::id())->paginate(10);
        return view('activity.index')->with('activities',$activity);
    }
}
