<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use DB;
class MenuController extends Controller
{
    //
    public function index()
    {
        $menu=DB::table('menu')->join('users','menu.created_by','users.id')
            ->select('menu.id','menu.name as name','menu.status','menu.harga','users.name as users')
            ->paginate(10);
        return view('menu.index')->with('menu',$menu);
    }

    public function show($id)
    {
        Menu::where('id',$id)->first();
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $data=[
            'name'=>$request->nama,
            'harga'=>$request->harga,
            'created_by'=>Auth::id(),
            'status'=>'ready'
        ];

        Menu::creat($data);
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $data=[
            'name'=>$request->nama,
            'harga'=>$request->harga,
            'updated_by'=>Auth::id(),
            'status'=>'ready'
        ];
        Menu::where('id',$request->id)->update($data);
        return redirect()->back();
    }

    public function updateStatus($id)
    {

    }
}
