<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    //
    protected $table='pesanan';

    protected $fillable=[
        'name','status','nomor_meja',
        'pembayaran_id','created_by','nomor_pesanan'
    ];

    public $timestamps=true;

    public function pembayaran()
    {
        return $this->belongsTo('App\Models\Pembayaran');

    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
