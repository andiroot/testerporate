<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPesanan extends Model
{
    //
    protected $table='detail_pesanan';

    protected $fillable=[
        'pesanan_id','menu_id','qty'
    ];

    public $timestamps=true;

    public function pesanan()
    {
        return $this->belongsTo('App\Models\Pesanan');
    }

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }
}
