<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function activity()
    {
        return $this->hasMany('App\Models\Activity');
    }

    public function createdPesanan()
    {
        return $this->hasMany('App\Models\DetailPesanan');
    }

    public function updatedPesanan()
    {
        return $this->hasMany('App\Models\DetailPesanan');
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Models\Pembayaran');
    }

    public function pesanan()
    {
        return $this->hasMany('App\Models\Pesanan');
    }


    public function role()
    {
        return $this->hasMany('App\Models\UserRole');
    }
}
