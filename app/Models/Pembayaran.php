<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    //
    protected $table='pembayaran';

    protected $fillable=[
        'crated_by','dibayar','kembalian',
        'discount'
    ];

    public $timestamps=true;

    public function created()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function pesanan()
    {
        return $this->hasMany('App\Models\Pesanan');
    }
}
