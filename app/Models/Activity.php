<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //

    protected $table='activity';

    protected $fillable=[
        'user_id','name'
    ];

    public $timstamps=true;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
