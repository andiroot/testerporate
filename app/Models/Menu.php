<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $table='menu';

    protected $fillable=[
        'name','status','harga'
        ,'created_by','updated_by'
    ];

    public $timestamps=true;

    public function created()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function updated()
    {
        return $this->belongsTo('App\Models\User');
    }
}
